## 설명
`merge request` 목표

## 영향을 받는 컴포넌트 목록
- 

## Merge Request 체크리스트
- [ ] a
- [ ] b
- [ ] c
- [ ] d

## 변경점 타입
- [ ] Bug fix
- [ ] New feature
- [ ] Breaking change

## 비기능 요구사항
- [ ] 코드 스타일 규칙
- [ ] 변경점 테스트
- [ ] 통과한 테스트
- [ ] 문서
