<!---

사용자스토리 작성 템플릿
- 사용자가 필요로 하는것이 무엇인지 요약합니다. 
- 사용자가 '누구'가 '무엇'을 '어떻게' 하는지 기술합니다.
- 인수 테스트 체크리스트로 DoD를 정리합니다.

--->

아무개 퍼소나 **사용자**는 **어떤** 목적을 가지고 **무엇**을 합니다.

### 인수 조건 (Acceptance Criteria)

1. [If I do A.]
1. [B should happen.]


#### 제약사항 또는 부연설명

1. Constraint 1;
1. Constraint 2;
1. Constraint 3.



### 참고 자료:

* 문서/목업/인터페이스: 
* Testing URL:
* Staging URL:


### 추가 정보

* 필요한 추가 정보를 기술

/label ~UserStory

